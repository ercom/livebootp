
citbx_module_dockerimg_define() {
    local prj_name=$(git remote -v \
        | gawk '
            match($0, /^origin\s.*:[\/]*(.*)\.git\s.*$/, ret) {
                print ret[1];
                exit;
            }
            match($0, /^origin\s.*:[\/]*(.*)\s.*$/, ret) {
                print ret[1];
                exit;
            }')
    local declare_opts=()
    if [ -n "$prj_name" ]; then
        declare_opts+=(-x "\"\$CI_REGISTRY/$prj_name\"")
    fi
    bashopts_declare -n CI_REGISTRY_IMAGE -l image-name -d "Registry image name" -t string "${declare_opts[@]}"
    bashopts_declare -n CI_COMMIT_TAG -l image-tag -d "Image tag" -t string -v "test"
    bashopts_declare -n USE_DIND_SERVICE -l use-dind -d "Use the dind docker service instead of the host docker instance" -t boolean
}

citbx_module_dockerimg_setup() {
    if [ $CITBX_UID -ne 0 ]; then
        DOCKER_GID=$(awk -F ':' '/docker:/ {print $3; exit;}' /etc/group)
        citbx_export DOCKER_GID
    fi
    CITBX_USER_GROUPS+=(docker)
    citbx_export CI_REGISTRY_IMAGE CI_COMMIT_TAG
    bashopts_process_option -n CI_REGISTRY_IMAGE -r
    if [ "$USE_DIND_SERVICE" == "false" ]; then
        CITBX_DISABLED_SERVICES+=(docker)
    fi
    pattern='\bdocker\b'
    if [[ "${CITBX_DISABLED_SERVICES[*]}" =~ $pattern ]]; then
        DOCKER_HOST="unix:///var/run/docker.sock"
        citbx_export DOCKER_HOST
    fi
}
